### 1.准备工作

- **Vagrant 2.3.4**

下载地址：[https://developer.hashicorp.com/vagrant/downloads](https://developer.hashicorp.com/vagrant/downloads)

- **VirtualBox 6.1.32**

下载地址：[https://www.virtualbox.org/wiki/Download_Old_Builds_6_1](https://www.virtualbox.org/wiki/Download_Old_Builds_6_1)

> **注意：**
> 
> 1. 推荐使用 VirtualBox 6.1.32 或之后的版本，旧版在Windows下可能与Hyper-V有冲突。
> 2. 不要使用 VirtualBox 7.0，Vagrant目前支持还不是很好。

### 2.Vagrant介绍

**Vagrant**是一个虚拟机管理工具，可以通过配置文件，批量、自动化的创建虚拟机，并提供一致性环境。
**Vagrant**不提供虚拟化，需要配合VirtualBox或VMware使用。
**Vagrant**使用box镜像创建虚拟机，box类似于docker image，box是提供一个预先配置好的环境，开箱即用。
使用Vagrant创建虚拟机非常简单：

```ruby
# 下载ubuntu 22.04的box文件到本地，并自动生成Vagrantfile
vagrant init ubuntu/jammy64
# 一键创建和启动虚拟机
vagrant up
```

Vagrant默认从[HashiCorp's Vagrant Cloud box catalog](https://vagrantcloud.com/boxes/search)下载镜像，由于国内网络的原因，下载速度非常慢，并且它不像dockerHub，国内有整站的镜像。
好在Ubuntu官方下载中心会发布基于ubuntu的box镜像(仅支持virtualbox)，因此我们可以从ubuntu的国内镜像中心下载。

```ruby
# vagrant init boxname url
vagrant init ubuntu/jammy64 
        \https://mirrors.tuna.tsinghua.edu.cn/ubuntu-cloud-images/jammy/current/jammy-server-cloudimg-amd64-vagrant.box
```

### 3.集群规划

用户名：`root`   密码：`123456`

| **主机名** | **IP地址** | **类型** |
| --- | --- | --- |
| k3s-server | 192.168.56.20 | Control Plane 控制平台 |
| k3s-agent1 | 192.168.56.21 | Node节点 |
| k3s-agent2 | 192.168.56.22 | Node节点 |

### 4.创建k3s集群

下载启动脚本到当前目录：[https://gitee.com/jeff-qiu/k8s-2hours/tree/master/vagrant-k3s](https://gitee.com/jeff-qiu/k8s-2hours/tree/master/vagrant-k3s)

- `bootstrap.sh`  Ubuntu系统设置脚本
- `Vagrantfile`  Vagrant创建虚拟机配置文件

创建虚拟机并自动部署k3s集群

```bash
vagrant up
```

k3s安装脚本和容器镜像均使用阿里云镜像仓库，避免网速不佳或无法下载的问题。

### 5.虚拟机IP

VirtualBox默认将`192.168.56.0/21`范围内的地址分配给虚拟机。可以使用以下命令查看IP地址范围。

```bash
VBoxManage list hostonlyifs
```

`Vagrantfile`中使用下面两个变量设置虚拟机的IP地址。

```ruby
#ip地址从192.168.56.20的开始递增
$ip_range   = "192.168.56."
$ip_start   = 20
```

如果`Vagrantfile`中的IP地址不在VirtualBox分配的范围内，会出现如下错误：
![image.png](../images/vagrant-ip-range.png)
请根据提示修改`Vagrantfile`。

### 6.虚拟机网络

![image.png](../images/vagrant-network.png)

Vagrant创建的虚拟机总是将第一个网卡设置为网络地址转换`(NAT)`，不可更改。
NAT模式下，虚拟机只能单向访问外部网络(通常用来访问互联网)，虚拟机之间相互隔离，无法互相访问，每个虚拟机的IP都是`10.0.2.15`。
我们在`Vagrantfile` 中通过`private_network`添加了一个`Host-Only`网络，虚拟机之间使用这个网络进行通信，宿主机也可以通过IP地址访问虚拟机。

```ruby
master.vm.network "private_network", ip: $master_ip
```

`Host-Only`顾名思义，虚拟机仅对宿主机可见，宿主机之外的主机无法访问虚拟机，虚拟机也无法访问外部网络(互联网)。

> **注意：**
> 
> 1. 网络地址转换**NAT**和**NAT**网络不一样。在VirtualBox官方文档中，网络地址转换的英文叫**NAT**，而NAT网络叫 **NAT network** 或 **NAT service**

![image.png](../images/vagrant-virtualbox-net.png)

> 2. NAT网络可以为每个虚拟机设置不同的IP地址，虚拟机之间可以互相访问，也可以访问互联网，但是无法从外部网络访问虚拟机。
> 3. 参考文档：[https://www.virtualbox.org/manual/ch06.html](https://www.virtualbox.org/manual/ch06.html)

### 7.flannel网络

K3s内置了`flannel`作为默认的网络插件(CNI)。
由于`flannel`默认是选择第一个网卡进行网络通信，Vagrant总是将虚拟机的第一个网卡设置为NAT（只能访问互联网，虚拟机之间无法互相访问）,因此会导致集群安装失败。
因此，在安装k3s时我们需要使用`--flannel-iface`将第二个网卡设置为`flannel`的默认网络。

```bash
curl -sfL https://rancher-mirror.oss-cn-beijing.aliyuncs.com/k3s/k3s-install.sh | \
          K3S_TOKEN=<your_token> INSTALL_K3S_MIRROR=cn \
          sh -s - --flannel-iface enp0s8
```

